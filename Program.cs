﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Kata3_FreddyFridays
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Number of Friday the 13ths in a given year - 2021.");
            Console.WriteLine(CountFridays13s(2021));
            Console.WriteLine("Max friday the 13ths for the period of current year to year 3000");
            // The out is a way get around multiple returns
            // The variables declared in the method params will be available to us in this scope.
            GetMaxFridays(out Dictionary<int, int> instances, out int maxFridays); 
            Console.WriteLine(maxFridays);
            Console.WriteLine("Years with the maximum number of Friday the 13ths");
            PrintAllMaxFridays(instances, maxFridays);
        }

        private static void PrintAllMaxFridays(Dictionary<int, int> instances, int maxFridays)
        {
            // Loop through all entries using foreach and KeyValuePair
            foreach (KeyValuePair<int, int> entry in instances)
            {
                if (entry.Value == maxFridays)
                {
                    Console.WriteLine(entry.Key);
                }
            }
        }

        private static void GetMaxFridays(out Dictionary<int, int> instances, out int maxFridays)
        {
            // Key is year, value is count
            instances = new Dictionary<int, int>();
            for (int i = DateTime.Now.Year; i <= 3000; i++)
            {
                instances.Add(i, CountFridays13s(i));
            }
            maxFridays = instances.Values.Max();
        }

        private static int CountFridays13s(int year)
        {
            if(year < DateTime.Now.Year || year > 3000)
            {
                throw new ArgumentException("Year must be between the current year and 3000");
            }
            int count = 0;
            // Loop through months
            for(int i = 1; i <= 12; i++)
            {
                // Create a date object with the 13th of each month
                DateTime date = new DateTime(year, i, 13);
                // Check if this date is a friday
                if (date.DayOfWeek == DayOfWeek.Friday)
                {
                    // Increment the count if it is
                    count++;
                }
            }
            return count;
        }
    }
}
