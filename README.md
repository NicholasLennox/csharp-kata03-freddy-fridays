# Kata: Freddy Fridays

[![standard-readme compliant](https://img.shields.io/badge/standard--readme-OK-green.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)

Create a method that takes in an integer between the current year and year 3000. The method then needs to count how many occurances of Friday the 13th appear in the given year. The application then needs to display this number in the console.

The application should then find the maximum number of Friday the 13ths in the years between the current year and 3000. The applciation then needs to print out all the years which have the same amount of Friday the 13ths as the maximum.

 The solution is implemented in C# using a Console Application in Visual Studio.

## Table of Contents

- [Install](#install)
- [Usage](#usage)
- [Maintainers](#maintainers)
- [Contributing](#contributing)
- [License](#license)

## Install

- Clone 
- Open in Visual Studio
- Build the application

## Usage

- Run the application from Visual Studio.

## Maintainers

[Nicholas Lennox](https://github.com/NicholasLennox)

## Contributing

Small note: If editing the README, please conform to the [standard-readme](https://github.com/RichardLitt/standard-readme) specification.

## License

MIT © 2021 Noroff Accelerate AS
